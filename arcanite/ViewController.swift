//
//  ViewController.swift
//  arcanite
//
//  Created by MacBookPro on 20/10/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mainLabel: UILabel!
    @IBAction func setText(_ sender: UIButton) {
        mainLabel.text! = "Hello, World!"
    }
    @IBAction func clearText(_ sender: UIButton) {
        mainLabel.text! = ""
    }
}

